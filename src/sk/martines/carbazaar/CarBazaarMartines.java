package sk.martines.carbazaar;

public class CarBazaarMartines {

    private final String name;

    private int capacity = 10;
    private Car[] cars = new Car[capacity];
    private int size = 0;

    public CarBazaarMartines(String name) {
        this.name = name;
    }

    public boolean addCar(Car newCar) {
        if (searchById(newCar.getId()) != null)
            return false;
        if (size < cars.length) {
            cars[size] = newCar;
            size++;
            return true;
        }

        cars[size] = newCar;
        size++;

        return true;
    }

    public void removeCar(Car a) {
        for (int i = 0; i < size; i++) {
            if (cars[i] == a) {
                cars[i] = cars[size - 1];
                cars[size - 1] = null;
                size--;
                break;
            }
        }
    }


    public Car searchById(int id) {
        for (int i = 0; i < size; i++) {
            if (cars[i].getId() == id) {
                return cars[i];
            }
        }
        return null;
    }

    public void searchByBrand(String brand) {
        for (int i = 0; i < size; i++) {
            if (cars[i].getBrand().equals(brand)) {
                cars[i].print();
            }
        }
    }

    public void print() {
        System.out.println(name);

        for (int i = 0; i < size; i++) {
            Car car = cars[i];
            car.print();
        }
    }

    public void save() {
    }

    public void load() {
    }
}
