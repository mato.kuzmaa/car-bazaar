package sk.martines.carbazaar;

public class Car {

    private final int id;

    private String color;
    private String brand;
    private String model;
    private boolean turbo;
    private boolean automatic;
    private int height;

    public Car(int id, String color, String brand, String model, boolean turbo, boolean automatic, int height) {
        this.id = id;
        this.color = color;
        this.brand = brand;
        this.model = model;
        this.turbo = turbo;
        this.automatic = automatic;
        this.height = height;
    }

    public void print() {
        System.out.println(id + " " + color + " " + brand + " " + model + " " + turbo + " " + automatic + " " + height);
    }

    public int getId() {
        return id;
    }

    public String getColor() {
        return color;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public boolean getTurbo() {
        return turbo;
    }

    public boolean getAutomatic() {
        return automatic;
    }

    public int getHeight() {
        return height;
    }
}
