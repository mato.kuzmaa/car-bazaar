package sk.martines.carbazaar;

public class CarTest {
    public static void main(String[] args) {

        Car a = new Car(1, "black", "Hyundai", "Tucson", true, false, 165);
        Car b = new Car(2, "yellow", "Kia", "Ceed", false, true, 130);
        Car c = new Car(3, "gray", "Audi", "A5", true, true, 120);
        Car d = new Car(4, "white", "BMW", "X3", true, true, 135);
        Car e = new Car(5, "white", "Peugeot", "3008", false, false, 160);

        Car[] cars = {
                a, b, c, d, e
        };

        searchById(cars, 2);
        searchByColor(cars, "white");
        searchByBrand(cars, "Hyundai");
        searchByModel(cars, "Ceed");
        searchByTurbo(cars, true);
        searchByAutomatic(cars, false);
        searchByHeight(cars, 165);

        maxOf(cars);
    }

    public static void searchById(Car[] cars, int id) {
        for (Car car : cars) {
            if (car.getId() == id) {
                car.print();
            }
        }
    }

    private static void searchByColor(Car[] cars, String color) {
        for (Car car : cars) {
            if (car.getColor().equals(color)) {
                car.print();
            }
        }
    }

    private static void searchByBrand(Car[] cars, String brand) {
        for (Car car : cars) {
            if (car.getBrand().equals(brand)) {
                car.print();
            }
        }
    }

    private static void searchByModel(Car[] cars, String model) {
        for (Car car : cars) {
            if (car.getModel().equals(model)) {
                car.print();
            }
        }
    }

    private static void searchByTurbo(Car[] cars, boolean turbo) {
        for (Car car : cars) {
            if (car.getTurbo() == turbo) {
                car.print();
            }
        }
    }

    private static void searchByAutomatic(Car[] cars, boolean automatic) {
        for (Car car : cars) {
            if (car.getAutomatic() == automatic) {
                car.print();
            }
        }
    }

    private static void searchByHeight(Car[] cars, int height) {
        for (Car car : cars) {
            if (car.getHeight() == height) {
                car.print();
            }
        }
    }

    private static void maxOf(Car[] cars) {
        Car max = cars[0];
        for (int i = 1; i < cars.length; i++) {

            if (cars[i].getHeight() > max.getHeight()) {
                max = cars[i];
            }
        }
        max.print();
    }
}
