package sk.martines.carbazaar;

import java.util.Scanner;

public class CarConsole {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        CarBazaarMartines x = new CarBazaarMartines("Martines bazaar");

        while (true) {
            System.out.println("Choose option:\n 1. Print all cars\n 2. Add new car\n 3. Find car\n 4. Remove car\n 5. Save cars\n 6. Load cars\n 0. exit");

            int option = Integer.parseInt(scanner.nextLine());

            if (option == 1) {
                x.print();
            } else if (option == 2) {
                Car a = readCar(scanner);
                x.addCar(a);
            } else if (option == 3) {
                System.out.println("Enter brand");
                String brand = scanner.nextLine();
                x.searchByBrand(brand);
            } else if (option == 4) {
                System.out.println("Enter id");
                int id = Integer.parseInt((scanner.nextLine()));
                Car a = x.searchById(id);
                if (a != null) {
                    x.removeCar(a);
                } else {
                    System.out.println("Car 404");
                }
            } else if (option == 5) {
                x.save();
                System.out.println("");
            } else if (option == 6) {
                x.load();
                System.out.println("");
            } else if (option == 0) {
                break;
            } else {
                System.out.println("Wrong option");
            }
        }
    }


    private static Car readCar(Scanner scanner) {

        System.out.println("Enter ID");
        int id = Integer.parseInt(scanner.nextLine());

        System.out.println("Enter color");
        String color = scanner.nextLine();

        System.out.println("Enter brand");
        String brand = scanner.nextLine();

        System.out.println("Enter model");
        String model = scanner.nextLine();

        System.out.println("Enter turbo");
        boolean turbo = Boolean.parseBoolean(scanner.nextLine());

        System.out.println("Enter automatic");
        boolean automatic = Boolean.parseBoolean(scanner.nextLine());

        System.out.println("Enter height");
        int height = Integer.parseInt(scanner.nextLine());

        return new Car(id, color, brand, model, turbo, automatic, height);
    }
}

